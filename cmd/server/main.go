package main

import (
	"fmt"
	"os"

	"github.com/urfave/cli/v2"

	"gitlab.com/varikin/romen/romen"
	"gitlab.com/varikin/romen/server"
)

// Version set at compile time
var Version = "dev"

func main() {
	app := cli.NewApp()
	app.Name = "romen"
	app.Usage = "Manage ROM files for RetroPie"
	app.Action = run
	app.Version = Version
	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:    "host",
			Value:   "0.0.0.0:1987",
			Usage:   "Host address",
			EnvVars: []string{"ROMEN_HOST"},
		},
		&cli.StringFlag{
			Name:    "enabled-path",
			Value:   "/home/pi/RetroPie/roms",
			Usage:   "Path to enabled ROMs",
			EnvVars: []string{"ROMEN_ENABLED_PATH"},
		},
		&cli.StringFlag{
			Name:    "available-path",
			Value:   "/home/pi/RetroPie/available-roms",
			Usage:   "Path to available ROMs",
			EnvVars: []string{"ROMEN_AVAILABLE_PATH"},
		},
	}

	if err := app.Run(os.Args); err != nil {
		fmt.Println(err.Error())
	}
}

func run(c *cli.Context) error {
	fmt.Println("Starting Romen; version " + c.App.Version)
	romenConfig, err := romen.NewRomen(c.String("enabled-path"), c.String("available-path"))
	s, err := server.NewServer(c.String("host"), romenConfig)
	if err != nil {
		return cli.Exit("Error starting server: "+err.Error(), 1)
	}
	if err = s.Serve(); err != nil {
		return cli.Exit(err.Error(), 2)
	}

	return nil
}
