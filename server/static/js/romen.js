$(document).ready(function() {
    enableDataTable();
});

function enableDataTable() {
    // Create an array with the values of all the checkboxes in a column
    $.fn.dataTable.ext.order['dom-checkbox'] = function(settings, column) {
        return this.api().column(column, {order:'index'}).nodes().map(function(td, i) {
            return $('input', td).prop('checked') ? '1' : '0';
        } );
    };

    $('.rom-list').DataTable({
        /*
         * Set the DOM for the table, using the default bootstrap layout
         * plus adding a console filter div to the top controls
         */
        dom: "<'row'<'col-sm-4'l><'#console-filter.col-sm-4'><'col-sm-4'f>>" +
             "<'row'<'col-sm-12'tr>>" +
             "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        // Defining all the columns with names
        columns: [
            {
                "name": "name",
                "data": "Name"
            },
            {
                "name": "console",
                "data": "Console"
            },
            {
                "name": "status",
                "data": "Enabled",
                "orderDataType": "dom-checkbox",
                "render": function(enabled, type, row) {
                    var checked = enabled ? "checked" : "";
                    return '<input' +
                        ' type="checkbox" ' +
                        ' value="' + row.Path + '"' +
                        checked +
                        ' data-toggle="toggle"' +
                        ' data-on="Playable"' +
                        ' data-off="Disabled"' +
                        ' class="rom-switch"' +
                        '>';
                }
            }
        ],
        ajax: {
            url: "/roms",
            dataSrc: "roms"
        },
        initComplete: setupConsoleFilter,
        drawCallback: watchSwitch
    });
}

function setupConsoleFilter() {
    var column = this.api().column("console:name");

    // Creating a select form that searches on change
    var select = $('<select class="form-control input-sm"><option value=""></option></select>')
        .on('change', function() {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            var term = val ? "^" + val + "$" : "";
            column.search(term, true, false).draw();
        });

    // Adding the consoles to the filter
    column.data().unique().sort().each(function(data, j) {
        select.append('<option value="' + data + '">' + data + '</option>')
    });
    $('<labal>Select Console: </label>').append(select).appendTo($('#console-filter'));
}

function watchSwitch() {
    /*
     * First we have we remove the old handlers, the add the new handlers.
     * Otherwise, multiple handlers fire if the onDraw event repaints the table.
     */
    var checkboxes = $('.rom-switch:checkbox');
    checkboxes.off('change');
    checkboxes.change(function() {
        var path = $(this).val();
        var method = $(this).is(":checked") ? "POST" : "DELETE";
        if (path) {
            $.ajax({
                "url": encodeURIComponent(path),
                "method": method,
                "data": {
                    "path": path
                }
            });
        }
    });
}
