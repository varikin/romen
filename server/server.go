package server

import (
	"gitlab.com/varikin/romen/romen"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"

	"embed"
	"html/template"
	"io/fs"
	"net/http"
	"net/url"
	"path"
	"time"
)

//go:embed static templates
var embeddedContent embed.FS

// Server configuration
type Server struct {
	Host   string
	engine *gin.Engine
	Romen  *romen.Config
}

// NewServer creates a new server with the given host
func NewServer(host string, romen *romen.Config) (*Server, error) {
	gin.SetMode(gin.ReleaseMode)
	s := Server{
		Host:   host,
		engine: gin.New(),
		Romen:  romen,
	}
	s.engine.Use(gin.Recovery())
	s.engine.Use(Ginrus(logrus.StandardLogger(), time.RFC3339))

	if err := s.initTemplates(); err != nil {
		return nil, err
	}

	static, err := fs.Sub(embeddedContent, "static")
	if err != nil {
		return nil, err
	}
	s.engine.StaticFS("/static", http.FS(static))
	s.engine.GET("/health", health)
	s.engine.GET("/about", renderTemplate("about.tmpl"))
	s.engine.GET("/", renderTemplate("index.tmpl"))
	s.engine.GET("/roms", s.romList)
	s.engine.POST("/:console/:name", s.enableRom)
	s.engine.DELETE("/:console/:name", s.disableRom)

	return &s, nil
}

func health(c *gin.Context) {
	data := gin.H{"status": "Romen is healthy"}
	status := http.StatusOK
	switch c.NegotiateFormat("application/json", "text/html") {
	case "text/html":
		c.HTML(status, "health.tmpl", data)
	case "application/json", "":
		c.JSON(status, data)
	}
}

func renderTemplate(template string) func(c *gin.Context) {
	return func(c *gin.Context) {
		c.HTML(http.StatusOK, template, nil)
	}
}

func (s *Server) romList(c *gin.Context) {
	roms := make([]*romen.Rom, len(s.Romen.Roms))
	i := 0
	for _, rom := range s.Romen.Roms {
		roms[i] = rom
		i++
	}
	c.JSON(http.StatusOK, gin.H{
		"roms": roms,
	})
}

func (s *Server) enableRom(c *gin.Context) {
	s.updateRom(c, s.Romen.EnableRom)
}

func (s *Server) disableRom(c *gin.Context) {
	s.updateRom(c, s.Romen.DisableRom)
}

func (s *Server) updateRom(c *gin.Context, update func(romPath string) error) {
	console := c.Param("console")
	name := c.Param("name")
	romPath, err := url.PathUnescape(path.Join(console, name))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"status": "error",
			"error":  err.Error(),
		})
		return
	}

	err = update(romPath)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"status": "error",
			"error":  err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": "success"})
}

func (s *Server) initTemplates() error {

	templates := template.New("Server Templates")
	templatesFs, err := fs.Sub(embeddedContent, "templates")
	if err != nil {
		return err
	}
	err = fs.WalkDir(templatesFs, ".", func(path string, d fs.DirEntry, err error) error {
		if d.IsDir() {
			return nil
		}
		logrus.WithField("template", path).Info("Loading template")
		content, err := fs.ReadFile(templatesFs, path)
		if err != nil {
			return err
		}

		_, err = templates.New(path).Parse(string(content))
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return err
	}

	s.engine.SetHTMLTemplate(templates)
	return nil
}

// Serve starts the server
func (s *Server) Serve() error {
	return s.engine.Run(s.Host)
}
