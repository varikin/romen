// Based on gin-gonic/contrib/ginerus but the bad import of Logrus
package server

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"

	"time"
)

// Ginrus returns a gin.HandlerFunc (middleware) that logs requests using logrus.
//
// Requests with errors are logged using logrus.Error().
// Requests without errors are logged using logrus.Info().
func Ginrus(logger *logrus.Logger, timeFormat string) gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		// some evil middlewares modify this values
		path := c.Request.URL.Path
		c.Next()

		end := time.Now()
		latency := end.Sub(start)
		end = end.UTC()

		entry := logger.WithFields(logrus.Fields{
			"ip":      c.ClientIP(),
			"latency": latency,
			"time":    end.Format(timeFormat),
		})

		if len(c.Errors) > 0 {
			entry.WithError(c.Err()).Error(c.Request.Method, " ", c.Writer.Status(), " ", path)
		} else {
			entry.Info(c.Request.Method, " ", c.Writer.Status(), " ", path)
		}
	}
}
