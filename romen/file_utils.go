package romen

import (
	"errors"
	"os"
	"path"
	"path/filepath"

	"github.com/spf13/afero"
)

var afs = afero.NewOsFs()
var dirFileMode = os.FileMode(0755)

func isDir(path string) error {
	isDir, err := afero.IsDir(afs, path)
	if err != nil {
		return errors.New("failed test if path is a directory: " + err.Error())
	}
	if !isDir {
		return errors.New("path is not a directory: " + path)
	}
	return nil
}

func listDirectories(path string) ([]os.FileInfo, error) {
	return list(path, func(file os.FileInfo) bool {
		return file.IsDir()
	})
}

func listFiles(path string) ([]os.FileInfo, error) {
	return list(path, func(file os.FileInfo) bool {
		return !file.IsDir()
	})
}

func list(path string, filter func(os.FileInfo) bool) ([]os.FileInfo, error) {
	files, err := afero.ReadDir(afs, path)
	if err != nil {
		return nil, errors.New("failed to list files: " + err.Error())
	}

	var results []os.FileInfo
	for _, file := range files {
		if filter(file) {
			results = append(results, file)
		}
	}
	return results, nil
}

func copyFile(source string, destination string) error {
	// Ensure the source file exists
	_, err := afs.Stat(source)
	if err != nil {
		return err
	}

	// Get the absolute path to the source
	fullSource, err := filepath.Abs(source)
	if err != nil {
		return err
	}

	// Ensure the destination path exists
	dir := path.Dir(destination)
	err = afs.MkdirAll(dir, dirFileMode)
	if err != nil {
		return err
	}

	// Symlink the file in place
	return os.Symlink(fullSource, destination)
}

func deleteFile(file string) error {
	return afs.Remove(file)
}
