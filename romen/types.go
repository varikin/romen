package romen

import (
	"errors"
)

type (
	// Config defines the top level config for Romen
	Config struct {
		EnabledPath   string
		AvailablePath string
		Roms          map[string]*Rom
	}

	Rom struct {
		Name    string
		Console string
		Path    string
		Enabled bool
	}
)

// NewRomen creates a new Config
func NewRomen(enabledPath string, availablePath string) (*Config, error) {
	if enabledPath == "" {
		return nil, errors.New("EnabledPath is required")
	}

	if err := isDir(enabledPath); err != nil {
		return nil, errors.New("EnabledPath is not a valid directory: " + err.Error())
	}

	if availablePath == "" {
		return nil, errors.New("AvailablePath is required")
	}

	if err := isDir(availablePath); err != nil {
		return nil, errors.New("AvailablePath is not a valid directory: " + err.Error())
	}

	config := &Config{
		EnabledPath:   enabledPath,
		AvailablePath: availablePath,
		Roms:          make(map[string]*Rom),
	}

	err := config.Load()
	if err != nil {
		return nil, err
	}

	return config, nil
}

func (r *Rom) Validate() error {
	if r.Name == "" {
		return errors.New("rom name is required")
	}
	if r.Console == "" {
		return errors.New("rom console is required")
	}
	if r.Path == "" {
		return errors.New("rom path is required")
	}
	return nil
}
