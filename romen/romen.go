package romen

import (
	"errors"
	"path"

	"github.com/sirupsen/logrus"
)

// Load the Consoles and ROMs for the Config
func (c *Config) Load() error {
	var err error
	roms, err := findRoms(c.AvailablePath)
	if err != nil {
		return errors.New("Failed to list available ROMs: " + err.Error())
	}
	enabledRoms, err := findRoms(c.EnabledPath)
	if err != nil {
		return errors.New("Failed to list enabled ROMs: " + err.Error())
	}

	for key, rom := range enabledRoms {
		if val, ok := roms[key]; ok {
			logrus.WithFields(logrus.Fields{
				"rom":     val.Path,
				"enabled": val.Enabled,
			}).Debug("Enabling rom")
			val.Enabled = true
		} else {
			// TODO This was enabled, but not available? Copy to available folder!
			rom.Enabled = true
			roms[key] = rom
		}
	}
	c.Roms = roms

	return nil
}

func findRoms(romPath string) (map[string]*Rom, error) {

	if err := isDir(romPath); err != nil {
		return nil, errors.New("ROM path is not a directory: " + err.Error())
	}

	// Get the list of emulator folders
	consoles, err := listDirectories(romPath)
	if err != nil {
		return nil, errors.New("failed to list console directories: " + err.Error())
	}

	roms := make(map[string]*Rom)
	for _, console := range consoles {
		files, err := listFiles(path.Join(romPath, console.Name()))
		if err != nil {
			return nil, errors.New("failed to list roms: " + err.Error())
		}

		for _, file := range files {
			rom := Rom{
				Name:    file.Name(),
				Console: console.Name(),
				Path:    path.Join(console.Name(), file.Name()),
				Enabled: false, // Assume false, reconciled later
			}
			roms[rom.Path] = &rom
		}
	}

	logrus.WithField("roms", len(roms)).Info("Loaded the ROMs")
	return roms, nil
}

func (c *Config) EnableRom(romPath string) error {
	if romPath == "" {
		return errors.New("path is required to enable a ROM")
	}

	logrus.WithField("path", romPath).Info("Enabling ROM")

	rom, ok := c.Roms[romPath]
	if !ok {
		return errors.New("ROM not found to enable: " + romPath)
	}
	if err := rom.Validate(); err != nil {
		return errors.New("ROM is invalid, cannot enable it: " + err.Error())
	}

	if rom.Enabled {
		return errors.New("ROM already enabled")
	}

	enabledPath := path.Join(c.EnabledPath, rom.Path)
	availablePath := path.Join(c.AvailablePath, rom.Path)
	err := copyFile(availablePath, enabledPath)
	// Set the ROM to enabled if the copy succeeded
	rom.Enabled = err == nil

	return err
}

func (c *Config) DisableRom(romPath string) error {
	if romPath == "" {
		return errors.New("path is required to disable a ROM")
	}

	logrus.WithField("path", romPath).Info("Disabling ROM")

	rom, ok := c.Roms[romPath]
	if !ok {
		return errors.New("ROM not found to disable: " + romPath)
	}
	if err := rom.Validate(); err != nil {
		return errors.New("ROM is invalid, cannot disable it: " + err.Error())
	}

	if !rom.Enabled {
		return errors.New("ROM already disabled")
	}

	file := path.Join(c.EnabledPath, rom.Path)
	err := deleteFile(file)
	// Set the ROM to disabled if the delete succeeded
	rom.Enabled = err != nil

	return err
}
