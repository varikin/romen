module gitlab.com/varikin/romen

go 1.16

require (
	github.com/gin-gonic/gin v1.7.2
	github.com/magefile/mage v1.11.0 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/afero v1.6.0
	github.com/urfave/cli/v2 v2.3.0
)
