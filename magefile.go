//+build mage

package main

import (
	"fmt"
	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
	"runtime"
)

// Build builds an un-versioned dev build
func Build() error {
	exe := binary(runtime.GOOS, runtime.GOARCH, "dev")
	return sh.Run("go", "build", "-o", exe, "./cmd/server")
}

// Run runs the dev build
func Run() error {
	mg.Deps(Build)
	exe := binary(runtime.GOOS, runtime.GOARCH, "dev")
	return sh.Run(exe, "--enabled-path", "enabled", "--available-path", "available")
}

// Test runs all the unit tests
func Test() error {
	return sh.Run("go", "test", "./...")
}

// Vet runs go vet for all the files
func Vet() error {
	return sh.Run("go", "vet", "./...")
}

func Check() {
	mg.SerialDeps(Vet, Test)
}

// ReleaseAll builds a release for all platforms with the given version.
func ReleaseAll(version string) {
	mg.Deps(
		mg.F(Release, "linux", "amd64", version),
		mg.F(Release, "darwin", "amd64", version),
		mg.F(Release, "windows", "amd64", version),
		mg.F(Release, "linux", "arm64", version),
		)
}
// Release builds a release for the given OS, arch, and version.
func Release(os, arch, version string) error {
	env := map[string]string{"GOOS": os, "GOARCH": arch}
	versionStr := "main.Version=" + version
	output := binary(os, arch, version)
	input := "./cmd/server"
	return sh.RunWith(env, "go", "build", "-ldflags", "-X", versionStr, "-o", output, input)
}

// binary returns the correct path for the binary.
func binary(os, arch, version string) string {
	suffix := ""
	if os == "windows" {
		suffix = ".exe"
	}

	if version == "dev" {
		return "bin/romen" + suffix
	} else {
		return fmt.Sprintf("bin/%s_%s/romen_%s%s", os, arch, version, suffix)
	}
}
