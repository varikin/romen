# Romen
### The ROM Manager

[![build status](https://gitlab.com/varikin/romen/badges/master/build.svg)](https://gitlab.com/varikin/romen/commits/master)
[![Download](https://img.shields.io/website-up-down-green-red/http/shields.io.svg?label=Download)](https://gitlab.com/varikin/romen/builds/artifacts/v0.0.3/download?job=package)

Have too many ROMs?
Feel like you need to delete a couple hundred ROMs just to find what you want to play?
But you don't want to delete ROMs, do you?
Oh no!

Romen will help.
It is a server that you run on the RetroPi (or other ROM game system).
With Romen, all the ROMs are put in one directory of `available` ROMs.
Then, through Romen, they can be enabled, which will add the ROM to the `enabled` directory.

![Romen](screenshot.png)

## Installation Romen

Romen is very new and doesn't have a good installation solution yet.
For now, just download the latest [build](https://gitlab.com/varikin/romen/builds/artifacts/v0.0.3/download?job=package)
This will download a zipfile that contains a Linux, Mac, and Windows build.
Just put this somewhere in your `PATH` and make it executable.

Or just clone and build. There is a Makefile that can build it, `make build`.
Might be the easiest :)

## Running Romen

```
romen --enabled-path /home/RetroPie/roms --available-path /home/RetroPie/all-roms
```
